const path = require('path')

var babelIncludes = [
  new RegExp(path.join(__dirname, '../node_modules/pubsweet-frontend/src')),
  new RegExp(path.join(__dirname, '../app')),
  new RegExp(path.join(__dirname, '../node_modules/pubsweet-.*'))
]

module.exports = babelIncludes
