## Overview  

Editoria is a book production platform, built with [Pubsweet](https://gitlab.coko.foundation/pubsweet/) and [Substance](http://substance.io/).  
This application is being developed by the [Coko Foundation](https://coko.foundation/), for the [University of California Press](http://www.ucpress.edu/).
<br>

## Installation  

First you need to clone this repository on your machine.  
```git clone git@gitlab.coko.foundation:yannisbarlas/editoria.git```  
or the ```https``` equivalent:  
```git clone https://gitlab.coko.foundation/yannisbarlas/editoria.git```  
<br>

Once you have, navigate to the project's root directory.  
```cd editoria```  
<br>

This application is being developed with node 6 in mind.  
If you use nvm for managing different node versions, the project includes an ```.nvmrc``` file that you can take advantage of.  
<br>

Install the latest version of the ```pubsweet``` command line tool.  
```npm install --global pubsweet```  
<br>

Install the project's dependencies.  
```npm install```  
<br>

Create a database.  
```pubsweet setupdb ./```  

If you want to create a database for a development environment, simply pass the ```--dev``` option to the above command.  
```pubsweet setupdb ./ --dev```  

Follow the instructions, create the administrator user and name your book.  
<br>

Once that is done, you can run the app like so:  
```pubsweet run```  
Or if you want the development environment:  
```pubsweet run --dev```  
<br>

## Set up  

Log in as an administrator, and click on the "Teams" link in the navigation bar.  
<br>

Create 3 teams:  
- Name the first "Production Editor", give it a type of "Production Editor all" and choose your book from the Collection dropdown.  
- Name the second "Copy Editors", give it a type of "Copy Editor update" and choose your book from the Collection dropdown.  
- Name the third "Authors", give it a type of "Author update" and choose your book from the Collection dropdown.  
<br>

You can now assign different users to different roles.  
If a user is a production editor, the user can then also edit user roles for all other users.  
