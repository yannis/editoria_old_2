global.CONFIG = { 'pubsweet-backend': '' }
global.PUBSWEET_COMPONENTS = []

global.mock = {
  data: require('./dataMock'),
  redux: require('./reduxMock')
}
