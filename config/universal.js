const editoriaMode = require('../app/authsome_editoria')

module.exports = {
  components: [
    'pubsweet-component-ink-backend',
    'pubsweet-component-ink-frontend',
    'pubsweet-component-login',
    'pubsweet-component-signup'
  ],
  inkBackend: {
    inkEndpoint: 'http://ink-api.coko.foundation',
    email: 'editoria@coko.foundation',
    password: 'editoria'
  },
  mode: editoriaMode,
  teams: {
    teamProduction: {
      name: 'Production Editor',
      permissions: 'all'
    },
    teamCopyEditor: {
      name: 'Copy Editor',
      permissions: 'update'
    },
    teamauthors: {
      name: 'Author',
      permissions: 'update'
    }
  },
  theme: 'ThemeEditoria'
}
