const path = require('path')

const universal = require('./universal')

module.exports = {
  authsome: {
    mode: universal.mode,
    teams: universal.teams
  },
  pubsweet: {
    components: universal.components
  },
  'pubsweet-backend': {
    dbPath: path.join(__dirname, '..', 'api', 'db'),
    secret: process.env.PUBSWEET_SECRET,
    API_ENDPOINT: '/api'
  },
  'pubsweet-component-ink-backend': universal.inkBackend,
  'pubsweet-frontend': {
    theme: universal.theme,
    routes: 'app/routes.jsx',
    navigation: 'app/components/Navigation/Navigation.jsx'
  }
}
