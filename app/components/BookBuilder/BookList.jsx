import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import * as Actions from 'pubsweet-frontend/src/actions'
import styles from './styles/bookList.local.scss'

export class BookList extends React.Component {
  constructor (props) {
    super(props)

    this._toggleModal = this._toggleModal.bind(this)

    this.state = {
      showModal: false
    }
  }

  _toggleModal () {
    this.setState({
      showModal: !this.state.showModal
    })
  }

  render () {
    const { book } = this.props
    const { showModal } = this.state
    let bookTitle = book ? book.title : 'Fetching...'
    let bookId = book ? book.id : ''

    return (
      <div className={styles.bookList + ' bootstrap'}>
        <div className='container col-lg-offset-2 col-lg-8'>

          <div className='col-lg-12'>
            <h1 className={styles.bookTitle}>
              Books
              <div className={styles.addBookBtn} onClick={this._toggleModal}>
                <a>add book</a>
              </div>
            </h1>
          </div>

          <div className='col-lg-12'>
            <div className={styles.bookContainer}>
              <h2>{bookTitle}</h2>
              <LinkContainer to={`/books/${bookId}/book-builder`}>
                <a href='#'
                  className={styles.editBook}
                  onClick={this._toggleModal} >
                  Edit
                </a>
              </LinkContainer>
            </div>

          </div>
        </div>

        <Modal
          className='modal'
          show={showModal}
          onHide={this._toggleModal}
          container={this}
        >

          <Modal.Header>
            <Modal.Title>
              Create a new book
            </Modal.Title>
          </Modal.Header>

          <Modal.Body>
            This feature is currently disabled.
          </Modal.Body>

          <Modal.Footer>
            <a className='modal-button modal-discard'
              onClick={this._toggleModal}>
              Close
            </a>
          </Modal.Footer>

        </Modal>

      </div>
    )
  }
}

BookList.propTypes = {
  book: React.PropTypes.object
}

function mapStateToProps (state) {
  return {
    book: state.collections[0]
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookList)
