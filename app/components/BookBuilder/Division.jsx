import _ from 'lodash'
import React from 'react'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'

import AddButton from './AddButton'
import Chapter from './Chapter'
import styles from './styles/bookBuilder.local.scss'

export class Division extends React.Component {
  constructor (props) {
    super(props)

    this._onAddClick = this._onAddClick.bind(this)
    this._onRemove = this._onRemove.bind(this)
    this._onMove = this._onMove.bind(this)
  }

  _onAddClick (group) {
    const { type, chapters, add, book } = this.props

    let newChapter = {
      book: book.id,

      subCategory: (type === 'body') ? group : 'component',
      division: type,
      alignment: {
        left: false,
        right: false
      },
      progress: {
        style: 0,
        edit: 0,
        review: 0,
        clean: 0
      },
      lock: null,

      index: chapters.length || 0,
      kind: 'chapter',
      title: (type === 'body') ? 'Untitled' : 'Choose Component',

      status: 'unpublished',
      author: '',
      source: '',
      comments: {},
      trackChanges: false
    }

    add(book, newChapter)
  }

  _onRemove (chapter) {
    const { chapters, remove, update, book } = this.props
    const deletedIndex = chapter.index

    remove(book, chapter)

    const chaptersToModify = _.filter(chapters, function (c) {
      return c.index > deletedIndex
    })

    _.forEach(chaptersToModify, function (c) {
      c.index -= 1
      update(book, c)
    })
  }

  // Reorder chapters
  _onMove (dragIndex, hoverIndex) {
    if (dragIndex === hoverIndex) { return }

    const { book, chapters, update } = this.props
    const dragChapter = chapters[dragIndex]

    let toUpdate = []

    if (dragIndex > hoverIndex) {
      const toModify = _.filter(chapters, function (c) {
        return c.index >= hoverIndex && c.index < dragIndex
      })
      _.forEach(toModify, function (c) {
        c.index += 1
        // update(book, c)
      })
      toUpdate = _.union(toUpdate, toModify)
    } else if (dragIndex < hoverIndex) {
      const toModify = _.filter(chapters, function (c) {
        return c.index <= hoverIndex && c.index > dragIndex
      })
      _.forEach(toModify, function (c) {
        c.index -= 1
        // update(book, c)
      })
      toUpdate = _.union(toUpdate, toModify)
    }

    dragChapter.index = hoverIndex
    // update(book, dragChapter)
    toUpdate.push(dragChapter)

    _.forEach(toUpdate, function (chapter) {
      update(book, chapter)
    })
  }

  render () {
    const { book, chapters, ink, outerContainer, roles, title, type, update } = this.props
    const { _onAddClick, _onRemove, _onMove } = this

    const chapterType = (type === 'body') ? 'chapter' : 'component'

    const chapterInstances = _.map(chapters, function (c, i) {
      return (
        <Chapter
          book={book}
          chapter={c}
          id={c.id}
          ink={ink}
          key={c.index}
          move={_onMove}
          no={i}
          outerContainer={outerContainer}
          remove={_onRemove}
          roles={roles}
          title={c.title}
          type={c.subCategory}
          update={update}
        />
      )
    })

    let addButtons
    if (type === 'body') {
      addButtons = (
        <span>
          <AddButton
            group='chapter'
            add={_onAddClick}
          />
          <AddButton
            group='part'
            add={_onAddClick}
          />
        </span>
      )
    } else {
      addButtons = (
        <AddButton
          group='component'
          add={_onAddClick}
        />
      )
    }

    const list = (
      <ul className={styles.sectionChapters}>
        { chapterInstances }
      </ul>
    )

    const emptyList = (
      <div className={styles.noChapters}>
        There are no {chapterType}s in this division.
      </div>
    )

    const displayed = chapters.length > 0 ? list : emptyList

    return (
      <div>
        <div className={styles.sectionHeader}>
          <div className={styles.sectionTitle}>
            <h1> { title } </h1>
          </div>

          { addButtons }

          <div className={styles.separator} />
        </div>

        <div id='displayed'>
          { displayed }
        </div>
      </div>
    )
  }
}

Division.propTypes = {
  add: React.PropTypes.func.isRequired,
  book: React.PropTypes.object.isRequired,
  chapters: React.PropTypes.array.isRequired,
  ink: React.PropTypes.func.isRequired,
  outerContainer: React.PropTypes.object.isRequired,
  remove: React.PropTypes.func.isRequired,
  roles: React.PropTypes.array,
  title: React.PropTypes.string.isRequired,
  type: React.PropTypes.string.isRequired,
  update: React.PropTypes.func.isRequired
}

export default DragDropContext(HTML5Backend)(Division)
