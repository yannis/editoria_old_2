import React from 'react'
import styles from '../styles/bookBuilder.local.scss'

export class UploadButton extends React.Component {
  constructor (props) {
    super(props)
    this.handleFileUpload = this.handleFileUpload.bind(this)
  }

  handleFileUpload (event) {
    event.preventDefault()

    const file = event.target.files[0]
    const {
      chapter,
      convertFile,
      toggleUpload,
      update
    } = this.props

    toggleUpload()

    convertFile(file).then(response => {
      chapter.source = response.converted
      update(chapter)
      toggleUpload()
    }).catch((error) => {
      console.error('INK error', error)
      toggleUpload()
    })
  }

  render () {
    const { accept, title, type } = this.props

    const input = (
      <input
        accept={accept}
        onChange={this.handleFileUpload}
        title={title}
        type={type}
      />
    )

    return (
      <div
        className={styles.btnFile}
        id='bb-upload'
      >
        Upload Word
        { input }
      </div>
    )
  }
}

UploadButton.propTypes = {
  accept: React.PropTypes.string.isRequired,
  chapter: React.PropTypes.object.isRequired,
  convertFile: React.PropTypes.func.isRequired,
  title: React.PropTypes.string.isRequired,
  toggleUpload: React.PropTypes.func.isRequired,
  type: React.PropTypes.string.isRequired,
  update: React.PropTypes.func.isRequired
}

export default UploadButton
