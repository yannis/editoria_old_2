import React from 'react'

import TextInput from '../../utils/TextInput'

class Title extends React.Component {
  save () {
    this.refs.chapterInput._save()
  }

  render () {
    const { isRenaming, onClickRename, onSaveRename, title } = this.props

    const input = (
      <TextInput
        className='edit'
        ref='chapterInput'
        onSave={onSaveRename}
        value={title}
      />
    )

    const plainTitle = (
      <h3 onDoubleClick={onClickRename}>
        { title }
      </h3>
    )

    if (isRenaming) return input
    return plainTitle
  }
}

Title.propTypes = {
  isRenaming: React.PropTypes.bool.isRequired,
  onClickRename: React.PropTypes.func.isRequired,
  onSaveRename: React.PropTypes.func.isRequired,
  title: React.PropTypes.string.isRequired
}

export default Title
