const chapter = {
  dropdownValues: {
    front: [
      'Table of Contents',
      'Introduction',
      'Preface',
      'Preface 1',
      'Preface 2',
      'Preface 3',
      'Preface 4',
      'Preface 5',
      'Preface 6',
      'Preface 7',
      'Preface 8',
      'Preface 9',
      'Preface 10'
    ],
    back: [
      'Appendix A',
      'Appendix B',
      'Appendix C'
    ]
  }
}

export { chapter }
