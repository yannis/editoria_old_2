import {
  BasePackage,
  // BlockquotePackage,
  CodePackage,
  EmphasisPackage,
  HeadingPackage,
  LinkPackage,
  // ListPackage,
  ParagraphPackage,
  PersistencePackage,
  ProseArticle,
  SpellCheckPackage,
  StrongPackage,
  SubscriptPackage,
  SuperscriptPackage
} from 'substance'

// My Elements
import CommentPackage from './elements/comment/CommentPackage'
import ExtractPackage from './elements/extract/ExtractPackage'
import ImagePackage from './elements/images/ImagePackage'
import LeftSwitchTextTypePackage from './elements/left_switch_text_type/LeftSwitchTextTypePackage'
import ListPackage from './elements/list/ListPackage'
import NotePackage from './elements/note/NotePackage'
import SourceNotePackage from './elements/source_note/SourceNotePackage'
import TrackChangePackage from './elements/track_change/TrackChangePackage'

let config = {
  name: 'simple-editor',
  configure: (config, options) => {
    config.defineSchema({
      name: 'prose-article',
      ArticleClass: ProseArticle,
      defaultTextType: 'paragraph'
    })

    config.import(BasePackage, {
      noBaseStyles: options.noBaseStyles
    })

    config.import(ParagraphPackage)
    config.import(HeadingPackage)
    config.import(EmphasisPackage)
    config.import(StrongPackage)
    config.import(SubscriptPackage)
    config.import(SuperscriptPackage)
    config.import(CodePackage)
    config.import(PersistencePackage)

    // config.import(CodeblockPackage)
    config.import(CommentPackage)
    config.import(ExtractPackage)
    config.import(ImagePackage)
    config.import(LeftSwitchTextTypePackage)
    config.import(LinkPackage)
    config.import(ListPackage)
    config.import(NotePackage)
    config.import(SourceNotePackage)
    config.import(SpellCheckPackage)
    config.import(TrackChangePackage)
  }
}

export default config
