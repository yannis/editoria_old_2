/* eslint react/prop-types: 0 */

import {
  Component,
  FontAwesomeIcon as Icon
} from 'substance'

class Comment extends Component {
  render ($$) {
    const active = this.props.active
    let name = this.props.user
    let text = this.props.text
    let resolveIconEl = $$('div')

    if (!active) {
      // preview
      if (text.length > 100) {
        text = text.substring(0, 100) + '...'
      }

      const resolveIcon = $$(Icon, { icon: 'fa-check' })
      const iconCircle = $$('div').addClass('sc-comment-resolve-icon-circle')
        .append(resolveIcon)

      resolveIconEl = $$('div')
        .addClass('sc-comment-resolve-icon')
        .attr('title', 'Resolve comment')
        .append(iconCircle)
        .on('click', this.resolve)
    }

    const nameEl = $$('span').addClass('comment-user-name').append(name, ' - ')
    const textEl = $$('span').addClass('comment-text').append(text)

    const entry = $$('div')
      .addClass('sc-comment-entry')
      .append(
        nameEl,
        textEl
      )

    return $$('div').addClass('single-comment-row')
      .append(
        entry,
        resolveIconEl
      )
  }

  resolve (e) {
    e.stopPropagation()
    const box = this.props.box
    box.resolve()
  }
}

export default Comment
