/* eslint react/prop-types: 0 */

import {
  Component,
  FontAwesomeIcon as Icon
} from 'substance'

import CommentList from './CommentList'

class CommentBox extends Component {
  constructor (props, args) {
    super(props, args)
    this.inputHeight = 0
  }

  // TODO -- fix class names
  render ($$) {
    const { active, entry } = this.props
    let comments = this.props.comments.data

    if (!active) {
      comments = comments.slice(0, 1) // preview
      if (comments.length === 0) return $$('div')
    }

    const commentList = $$(CommentList, {
      active: active,
      box: this,
      comments: comments
    })

    const box = $$('li')
      .attr('data-comment', entry.id)
      .addClass('sc-comment-pane-item')
      .addClass('animation')
      .append(commentList)
      .on('click', this.makeActive)

    let resolve = $$('div')

    if (active) {
      if (comments.length > 0) {
        const resolveIcon = $$(Icon, { icon: 'fa-check' })

        resolve = $$('button')
          .append(resolveIcon)
          .attr('title', 'Resolve')
          .addClass('comment-resolve')
          .on('click', this.resolve)
      }

      const textarea = $$('textarea')
        .attr('rows', '1')
        .ref('commentReply')
        .attr('id', entry.id)
        .on('keydown', this.onKeyDown)
        .on('input', this.textAreaAdjust)

      const replyIcon = $$(Icon, { icon: 'fa-plus' })

      // TODO -- refactor classes!!!
      const reply = $$('button')
        .attr('disabled', true)
        .attr('title', 'Reply')
        .addClass('comment-reply')
        .append(replyIcon)
        .ref('commentReplyButton')
        .on('click', this.reply)

      const inputBox = $$('div')
        .addClass('sc-new-comment')
        .append(textarea, reply, resolve)

      box
        .addClass('sc-comment-active')
        .append(inputBox)
    }

    return box
  }

  textAreaAdjust (event) {
    const value = this.refs.commentReply.getValue()
    let replyBtn = this.refs.commentReplyButton

    if (value.trim().length > 0) {
      replyBtn.el.removeAttr('disabled')
    } else {
      replyBtn.el.attr('disabled', 'true')
    }

    var parent = this.props.parent
    var textArea = event.path[0]

    textArea.style.height = '1px'
    textArea.style.height = (textArea.scrollHeight) + 'px'
    parent.setTops()
  }

  reply () {
    const comments = this.props.comments
    const id = this.props.entry.id
    const provider = this.getProvider()
    const user = this.props.user.username

    let textarea = this.refs.commentReply
    const text = textarea.getValue()

    comments.data.push({
      user: user,
      text: text
    })

    this.update(comments)

    textarea.setValue('')
    this.rerenderBoxList()
    provider.focusTextArea(id)
  }

  update (comments) {
    const id = this.props.entry.id
    const parent = this.props.parent

    parent.update(id, comments)
  }

  makeActive () {
    const id = this.props.entry.id
    const provider = this.getProvider()

    const activeEntry = provider.getActiveEntry()
    if (activeEntry === id) return

    provider.setActiveEntry(id)
    provider.focusTextArea(id)
  }

  resolve () {
    const id = this.props.entry.id
    const provider = this.getProvider()

    provider.resolveComment(id)
  }

  getProvider () {
    return this.context.commentsProvider
  }

  // sends the event to trigger a rerender of the whole box list
  // cannot simply rerender this box, as its height might have changed
  // and the boxes underneath might need to move
  rerenderBoxList () {
    const provider = this.getProvider()
    provider.emit('comments:updated')
  }

  onKeyDown (e) {
    if (e.keyCode === 27) { // escape
      const provider = this.getProvider()

      provider.removeActiveEntry()
      provider.removeEmptyComments()
    }

    if (e.keyCode === 13) { // enter
      const textAreaValue = this.refs.commentReply.getValue()
      if (textAreaValue.trim().length > 0) {
        this.reply()
      }
    }
  }
}

export default CommentBox
