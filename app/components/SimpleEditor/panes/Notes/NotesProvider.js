import _ from 'lodash'

import { TOCProvider } from 'substance'

class NotesProvider extends TOCProvider {
  computeEntries () {
    const doc = this.getDocument()
    const nodes = doc.getNodes()

    // get all notes from the document
    const notes = _.pickBy(nodes, function (value, key) {
      return value.type === 'note'
    })

    const entries = this.sortNodes(notes)
    return entries
  }

  sortNodes (nodes) {
    let notes = _.clone(nodes)
    const doc = this.getDocument()
    const container = doc.get('body')

    // sort notes by
    //   the index of the containing block
    //   their position within that block

    notes = _.map(notes, function (note) {
      const blockId = note.path[0]
      const blockPosition = container.getPosition(blockId)
      const nodePosition = note.start.offset

      return {
        id: note.id,
        content: note['note-content'],
        blockPosition: blockPosition,
        nodePosition: nodePosition,
        node: note
      }
    })

    return _.sortBy(notes, ['blockPosition', 'nodePosition'])
  }
}

NotesProvider.tocTypes = ['note']

export default NotesProvider
