import { Component } from 'substance'

class Notes extends Component {
  // use toc:updated to avoid rewriting TOCProvider's this.handleDocumentChange
  didMount () {
    this.context.editorSession.onUpdate('document', this.onNotesUpdated, this)
  }

  render ($$) {
    const provider = this.getProvider()
    const entries = provider.getEntries()
    let self = this

    const listItems = entries.map(function (entry, i) {
      let extractedElement = ''
      if (entry.content) {
        extractedElement = self.parseEntryContent($$, entry.content)
        return extractedElement
          .attr('data-id', entry.id)
          .addClass('sc-notes-footer-item')
      }
      return $$('li')
        .attr('data-id', entry.id)
        .addClass('sc-notes-footer-item')
        .append(extractedElement)
    })

    if (listItems.length === 0) return $$('div')
    return $$('ol')
        .addClass('sc-notes-footer')
        .append(listItems)
  }

  parseEntryContent ($$, content) {
    let parser = new DOMParser()
    let parsedContent = parser.parseFromString(content, 'text/html').body
    let parentElement = parsedContent.childNodes[0]
    let children = parentElement.childNodes
    let constructedElement = $$('li')

    for (let i = 0; i < children.length; i++) {
      if (children[i].nodeName === '#text') {
        constructedElement.append(children[i].data)
      } else {
        let contructedChildElement = $$(children[i].nodeName)
        // Case of nested styling, first contruct the sub child node
        if (children[i].children.length === 1) {
          let contructedSubChildElement = $$(children[i].children[0].nodeName)
          this.assignVirtualElementClass(children[i].children[0], contructedSubChildElement)
          this.assignVirtualElementClass(children[i], contructedChildElement)

          contructedSubChildElement.append(children[i].children[0].innerText)
          contructedChildElement.append(contructedSubChildElement)
        } else {
          this.assignVirtualElementClass(children[i], contructedChildElement)
          contructedChildElement.append(children[i].innerText)
        }

        constructedElement.append(contructedChildElement)
      }
    }

    return constructedElement
  }

  assignVirtualElementClass (DOMElement, virtualElement) {
    switch (DOMElement.nodeName) {
      case 'STRONG':
        virtualElement.addClass('sc-strong')
        break
      case 'EM':
        virtualElement.addClass('sc-emphasis')
        break
    }
  }

  getProvider () {
    return this.context.notesProvider
  }

  onNotesUpdated (change) {
    const notesProvider = this.getProvider()
    notesProvider.handleDocumentChange(change)
    this.rerender()
  }

  dispose () {
    const provider = this.getProvider()
    provider.off(this)
  }
}

export default Notes
