import { HTMLImporter } from 'substance'

import SimpleArticle from './SimpleArticle'

class SimpleImporter extends HTMLImporter {
  constructor (config) {
    super({
      schema: config.schema,
      converters: config.converters,
      DocumentClass: SimpleArticle
    })
  }

  // Takes an HTML string.
  convertDocument (bodyEls) {
    // handle INK imports
    // console.log(bodyEls)
    if (bodyEls.tagName === 'html') {
      // const body = bodyEls.find('div.docx-body')
      const body = bodyEls.find('body')
      if (body) bodyEls = body.childNodes
    }

    // Just to make sure we always get an array of elements
    if (!bodyEls.length) bodyEls = [bodyEls]

    this.convertContainer(bodyEls, 'body')
  }

  // TODO -- check substance's implementation of overlapping annotations

  // override substance's internal function to allow for overlapping
  // annotations, without adhering to an expand / fuse mode
  _createInlineNodes () {
    var state = this.state
    var doc = state.doc

    /*
      substance will break overlapping annotations of the same type into
      pieces like this:

      <anno id=1>
          <anno id=2></anno>
      </anno>
      <anno id=2>
      </anno>

      when the importer finds the duplicate annotation id, it will remove
      the first one altogether as a node from the doc

      here, we are forcing it to keep these duplicates and merge them
    */

    state.inlineNodes.forEach(function (node) {
      if (doc.get(node.id)) {
        const existing = doc.get(node.id)
        const newOne = node

        doc.delete(node.id)

        // only for comments
        if (node.id.split('-')[0] === 'comment') {
          // if they are adjacent and have the same id, merge the offsets
          if (
            existing.startOffset === newOne.endOffset ||
            existing.endOffset === newOne.startOffset
          ) {
            node.startOffset = Math.min(existing.startOffset, newOne.startOffset)
            node.endOffset = Math.max(existing.endOffset, newOne.endOffset)

            doc.create(node)
          }
        } else {
          doc.create(node) // TODO -- refactor
        }
      } else {
        doc.create(node)
      }
    })
  }
}

export default SimpleImporter
