import {
  ProseEditor,
  Toolbar
} from 'substance'

import ContainerEditor from '../ContainerEditor'

class MiniEditor extends ProseEditor {
  render ($$) {
    const el = $$('div').addClass('sc-mini-editor')
    let toolbar = this._renderMiniToolbar($$)
    let editor = this._renderEditor($$)
    let SplitPane = this.componentRegistry.get('split-pane')
    let ScrollPane = this.componentRegistry.get('scroll-pane')

    const contentPanel = $$(ScrollPane, {
      name: 'miniEditorContentPanel',
      scrollbarPosition: 'right'
    })
      .append(editor)
      .attr('id', 'content-panel')
      .ref('miniEditorContentPanel')

    el.append(
      $$(SplitPane, { splitType: 'horizontal' })
        .append(toolbar, contentPanel)
    )

    return el
  }

  _renderMiniToolbar ($$) {
    let commandStates = this.commandManager.getCommandStates()
    return $$('div').addClass('se-toolbar-wrapper').append(
      $$(Toolbar, {
        commandStates: commandStates,
        toolGroups: ['document', 'annotations']
      }).ref('mini_toolbar')
    )
  }

  _renderEditor ($$) {
    return $$(ContainerEditor, {
      editorSession: this.editorSession,
      containerId: 'body',
      spellcheck: 'native'
    }).ref('mini_body')
  }
}

export default MiniEditor
