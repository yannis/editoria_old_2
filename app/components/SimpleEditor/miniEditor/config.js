import {
  BasePackage,
  EmphasisPackage,
  ParagraphPackage,
  PersistencePackage,
  ProseArticle,
  StrongPackage,
  SpellCheckPackage

} from 'substance'

let config = {
  name: 'simple-editor',
  configure: (config, options) => {
    config.defineSchema({
      name: 'prose-article',
      ArticleClass: ProseArticle,
      defaultTextType: 'paragraph'
    })

    config.import(BasePackage, {
      noBaseStyles: options.noBaseStyles
    })
    config.import(ParagraphPackage)
    config.import(EmphasisPackage)
    config.import(StrongPackage)
    config.import(PersistencePackage)
    config.import(SpellCheckPackage)
  }
}

export default config
