import { each, filter, find, union } from 'lodash'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as Actions from 'pubsweet-frontend/src/actions'

import SimpleEditor from './SimpleEditor'

export class SimpleEditorWrapper extends React.Component {
  constructor (props) {
    super(props)

    this.fileUpload = this.fileUpload.bind(this)
    this.save = this.save.bind(this)
    this.update = this.update.bind(this)
  }

  componentWillMount () {
    const { getCollections, getFragments } = this.props.actions

    getCollections().then(result => {
      getFragments(result.collections[0])
    })

    const { user } = this.props
    user.roles = this.getRoles()
  }

  render () {
    const { book, fragment, history, user } = this.props

    return (
      <SimpleEditor
        book={book}
        fileUpload={this.fileUpload}
        fragment={fragment}
        history={history}
        onSave={this.save}
        update={this.update}
        user={user}
      />
    )
  }

  fileUpload (file) {
    const { fileUpload } = this.props.actions
    return fileUpload(file)
  }

  getRoles () {
    const { user, book } = this.props

    const teams = filter(user.teams, (t) => {
      return t.object.id === book.id
    })

    let roles = []
    const addRole = role => { roles = union(roles, [role]) }

    if (user.admin) addRole('admin')

    each(teams, (team) => {
      const name = team.teamType.name
      const modified = name.trim().toLowerCase().replace(' ', '-')
      addRole(modified)
    })

    return roles
  }

  save (source, callback) {
    const { fragment } = this.props
    fragment.source = source

    return this.update(fragment)
  }

  update (newChapter) {
    const { book } = this.props
    const { updateFragment } = this.props.actions
    return updateFragment(book, newChapter)
  }
}

// TODO -- review required props
SimpleEditorWrapper.propTypes = {
  actions: React.PropTypes.object.isRequired,
  book: React.PropTypes.object.isRequired,
  fragment: React.PropTypes.object,
  history: React.PropTypes.object.isRequired,
  user: React.PropTypes.object.isRequired,
  update: React.PropTypes.func
}

const mapStateToProps = (state, ownProps) => {
  const bookId = ownProps.params.bookId
  const book = find(state.collections, (c) => {
    return c.id === bookId
  })

  const fragmentId = ownProps.params.fragmentId
  const fragment = state.fragments[fragmentId]

  const user = state.currentUser.user

  return { fragment, book, user }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SimpleEditorWrapper)
