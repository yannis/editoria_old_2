// 'use strict'
//
// var ListComponent = require('substance/packages/list/ListComponent')
// var DialogueHtmlConverter = require('./DialogueHTMLConverter')
// var DialogueItemComponent = require('./DialogueItemComponent')
//
// function DialogueComponent () {
//   DialogueComponent.super.apply(this, arguments)
// }
//
// DialogueComponent.Prototype = function () {
//   this.render = function ($$) {
//     return DialogueHtmlConverter.render(this.props.node, {
//       createListElement: function (list) {
//         var tagName = 'dl'
//         return $$(tagName)
//           .attr('data-id', list.id)
//           .addClass('sc-dialogue')
//       },
//       renderListItem: function (item) {
//         return $$(DialogueItemComponent, { node: item })
//       }
//     })
//   }
//
//   this.onItemsChanged = function () {
//     this.rerender()
//   }
// }
//
// ListComponent.extend(DialogueComponent)
//
// module.exports = DialogueComponent
