// import { includes } from 'lodash'
//
// var Component = require('substance/ui/Component')
// var TextProperty = require('substance/ui/TextPropertyComponent')
//
// function DialogueItemComponent () {
//   DialogueItemComponent.super.apply(this, arguments)
// }
//
// DialogueItemComponent.Prototype = function () {
//   this.render = function ($$) {
//     var node = this.props.node
//
//     var el = $$('div')
//       .addClass('sc-dialogue-item')
//       .addClass('sm-' + node.listType)
//       .attr('data-id', this.props.node.id)
//       .append($$(TextProperty, {
//         path: [this.props.node.id, 'content']
//       })
//     )
//
//     return el
//   }
//
//   this.didMount = function () {
//     // HACK if the items were wrapped in a dl list then I could maybe
//     // achieve the same thing with css rules only
//     var el = this.el
//     var previous = el.el.previousSibling
//
//     var className = 'sc-dialogue-item-q'
//
//     if (previous && includes(previous.classList, 'sc-dialogue-item-q')) {
//       className = 'sc-dialogue-item-a'
//     }
//
//     el.addClass(className)
//   }
// }
//
// Component.extend(DialogueItemComponent)
//
// module.exports = DialogueItemComponent
