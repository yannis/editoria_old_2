// var DialogueItem = require('./DialogueItem')
// var DialogueItemComponent = require('./DialogueItemComponent')
// var DialogueEditing = require('./DialogueEditing')
//
// var ListMacro = require('substance/packages/list/ListMacro')
//
// module.exports = {
//   name: 'dialogue-item',
//   configure: function (config, options) {
//     config.addNode(DialogueItem)
//     config.addComponent(DialogueItem.static.name, DialogueItemComponent)
//
//     config.addTextType({
//       name: 'dialogue-item',
//       data: { type: 'dialogue-item' }
//     })
//
//     config.addStyle(__dirname, '_dialogue.scss')
//     config.addEditingBehavior(DialogueEditing)
//
//     if (options.enableMacro) {
//       config.addMacro(ListMacro)
//     }
//
//     config.addLabel('dialogue', {
//       en: 'Dialogue'
//     })
//
//     config.addLabel('dialogue-item', {
//       en: 'Dialogue'
//     })
//   }
// }
