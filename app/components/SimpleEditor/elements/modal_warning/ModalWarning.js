import { each } from 'lodash'
import { Modal, FontAwesomeIcon as Icon } from 'substance'

class ModalWarning extends Modal {
  constructor (props) {
    super(props)

    this.parent.on('send:route', this._getNextRoute, this)
    this.route = ''
    this.backButton = false
  }

  render ($$) {
    let el = $$('div')
        .addClass('sc-modal')

    const closeButton = $$(Icon, { icon: 'fa-close' })
          .addClass('sc-close-modal')
          .on('click', this._closeModal)

    const modalHeader = $$('div')
         .addClass('sc-modal-header')
         .append('Unsaved Content')
         .append(closeButton)

    const modalMessage = $$('div')
          .addClass('sc-modal-body')
          .append('Are you sure you want to exit the chapter without saving ?')

    const saveExit = $$('button')
          .addClass('sc-modal-button')
          .addClass('sc-modal-button-save-exit')
          .append('save & exit')
          .on('click', this._saveExitWriter)

    const exit = $$('button')
          .addClass('sc-modal-button')
          .addClass('sc-modal-button-exit')
          .append('exit')
          .on('click', this._exitWriter)

    const modalActionsContainer = $$('div')
          .addClass('sc-modal-actions')
          .append(saveExit)
          .append(exit)

    if (this.props.width) {
      el.addClass('sm-width-' + this.props.width)
    }

    el.append(
      $$('div').addClass('se-body')
      .append(modalHeader)
      .append(modalMessage)
      .append(modalActionsContainer)
    )

    return el
  }

  _getNextRoute (args) {
    this.route = args.location
    this.backButton = args.back
  }

  _closeModal () {
    this.send('closeModal')
  }

  _saveExitWriter () {
    this.context.editor.editorSession.save()
    if (this.backButton) {
      setTimeout(() => { this.context.editor.props.history.go(-2) }, 200)
    } else {
      setTimeout(() => { this.context.editor.props.history.push(this.route) }, 200)
    }
    this._closeModal()
  }

  // TODO: Hack Check why cannot rerender editor so can push to url
  // if you  save document session everything rerenders
  _exitWriter () {
    each(this.context.editor.editorSession._history.doneChanges, key => {
      this.context.editor.editorSession.undo()
    })

    this.context.editor.editorSession.save()
    if (this.backButton) {
      setTimeout(() => { this.context.editor.props.history.go(-2) }, 200)
    } else {
      // TODO: Hack Check why cannot rerender editor so can push to url
      setTimeout(() => { this.context.editor.props.history.push(this.route) }, 200)
    }
    this._closeModal()
  }
}

export default ModalWarning
