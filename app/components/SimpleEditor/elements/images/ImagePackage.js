import ImageNode from './ImageNode'
import ImageComponent from './ImageComponent'
import ImageHTMLConverter from './ImageHTMLConverter'
import InsertImageCommand from './InsertImageCommand'
import InsertImageTool from './InsertImageTool'
import ImageFileProxy from './ImageFileProxy'

export default {
  name: 'image',
  configure: function (config) {
    config.addNode(ImageNode)
    config.addComponent('image', ImageComponent)
    config.addConverter('html', ImageHTMLConverter)
    config.addCommand('insert-image', InsertImageCommand)
    config.addTool('insert-image', InsertImageTool, { toolGroup: 'annotations' })
    config.addIcon('insert-image', { 'fontawesome': 'fa-image' })
    config.addLabel('image', { en: 'Image' })
    config.addLabel('insert-image', { en: 'Insert image' })
    config.addFileProxy(ImageFileProxy)
  },
  ImageNode: ImageNode,
  ImageComponent: ImageComponent,
  ImageHTMLConverter: ImageHTMLConverter,
  InsertImageCommand: InsertImageCommand,
  InsertImageTool: InsertImageTool
}
