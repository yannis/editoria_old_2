import { AnnotationComponent } from 'substance'

class CommentComponent extends AnnotationComponent {
  constructor (parent, props) {
    super(parent, props)
    this.active = props.node.active
  }

  render ($$) {
    const { node } = this.props
    const active = node.active

    var el = $$('span')
      .attr('data-id', this.props.node.id)
      .addClass(this.getClassNames())

    // TODO -- needed?
    if (this.props.node.highlighted) {
      el.addClass('sm-highlighted')
    }

    if (active) {
      el.addClass('sc-comment-active')
    }

    el.append(this.props.children)
    // el.on('click', this.focus)

    return el
  }

  // TODO -- this was shouldRerender, but that stopped working. why?
  shouldRedraw (newProps) {
    if (this.hasNodeChanged()) {
      this.active = this.props.node.active
      this.rerender()
    }
  }

  // put here all conditions for the node to change
  hasNodeChanged () {
    const { node } = this.props

    if (this.active !== node.active) return true
    return false
  }

  didMount () {
    const provider = this.getProvider()
    provider.on('comments:updated', this.shouldRedraw, this)
  }

  getProvider () {
    return this.context.commentsProvider
  }

  // focus () {
  //   var id = this.props.node.id
  //   var provider = this.context.commentsProvider
  //
  //   provider.focusTextArea(id)
  // }
}

export default CommentComponent
