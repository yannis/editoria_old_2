import ResolvedComment from './ResolvedComment'
import ResolvedCommentCommand from './ResolvedCommentCommand'
import ResolvedCommentHTMLConverter from './ResolvedCommentHTMLConverter'

export default {
  name: 'resolved-comment',
  configure: function (config) {
    config.addNode(ResolvedComment)

    config.addCommand(ResolvedComment.type, ResolvedCommentCommand, { nodeType: ResolvedComment.type })
    config.addConverter('html', ResolvedCommentHTMLConverter)
  }
}
