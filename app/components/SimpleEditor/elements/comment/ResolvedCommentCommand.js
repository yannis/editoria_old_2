import { AnnotationCommand } from 'substance'

class ResolvedCommentCommand extends AnnotationCommand {}

ResolvedCommentCommand.type = 'resolved-comment'

export default ResolvedCommentCommand
