import {AnnotationCommand} from 'substance'

class CommentCommand extends AnnotationCommand {}

CommentCommand.type = 'comment'

export default CommentCommand
