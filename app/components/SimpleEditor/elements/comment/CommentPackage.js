import Comment from './Comment'
import CommentBubble from './CommentBubble'
import CommentCommand from './CommentCommand'
import CommentComponent from './CommentComponent'
import CommentHTMLConverter from './CommentHTMLConverter'
import ResolvedCommentPackage from './ResolvedCommentPackage'

export default {
  name: 'comment',
  configure: function (config, {
    disableCollapsedCursor, // TODO -- should delete?
    toolGroup
  }) {
    config.import(ResolvedCommentPackage)

    config.addNode(Comment)

    config.addComponent(Comment.type, CommentComponent)
    config.addConverter('html', CommentHTMLConverter)

    config.addCommand(Comment.type, CommentCommand, {
      disableCollapsedCursor,  // TODO -- same as above
      nodeType: Comment.type
    })

    config.addTool('comment', CommentBubble, {
      toolGroup: 'overlay'
    })

    config.addIcon('comment', { 'fontawesome': 'fa-comment' })
    config.addLabel('comment', {
      en: 'Comment'
    })
  }
}
