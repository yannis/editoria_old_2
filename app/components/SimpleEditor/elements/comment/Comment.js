import { PropertyAnnotation } from 'substance'

class Comment extends PropertyAnnotation {}

Comment.type = 'comment'

export default Comment
