import { PropertyAnnotation } from 'substance'

class SourceNote extends PropertyAnnotation {}

SourceNote.type = 'source-note'

export default SourceNote
