import SourceNote from './SourceNote'
import SourceNoteCommand from './SourceNoteCommand'
import SourceNoteHTMLConverter from './SourceNoteHTMLConverter'
import SourceNoteTool from './SourceNoteTool'

export default {
  name: 'source-note',
  configure: function (config) {
    config.addNode(SourceNote)

    config.addConverter('html', SourceNoteHTMLConverter)

    config.addCommand(SourceNote.type, SourceNoteCommand, { nodeType: SourceNote.type })
    config.addTool('source-note', SourceNoteTool, { target: 'annotations' })

    config.addIcon('source-note', { 'fontawesome': 'fa-book' })
    config.addLabel('source-note', {
      en: 'Source Note'
    })
  }
}
