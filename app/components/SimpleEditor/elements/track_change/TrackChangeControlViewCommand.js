import {Command} from 'substance'

class TrackChangeControlViewCommand extends Command {
  getCommandState (params) {
    let newState = {
      active: false,
      disabled: false
    }

    return newState
  }

  // TODO -- review
  execute (params, context) {
    const surface = context.surfaceManager.getSurface('body')
    surface.send('trackChangesViewToggle')
    surface.rerender()
    return true
  }
}

TrackChangeControlViewCommand.type = 'track-change-toggle-view'

export default TrackChangeControlViewCommand
