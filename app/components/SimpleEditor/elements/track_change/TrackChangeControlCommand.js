import {Command} from 'substance'

class TrackChangeControlCommand extends Command {
  getCommandState (params) {
    let newState = {
      disabled: false,
      active: false
    }

    return newState
  }

  execute (params, context) {
    const surface = context.surfaceManager.getSurface('body')
    surface.send('trackChangesUpdate')
    return true
  }
}

TrackChangeControlCommand.type = 'track-change-enable'

export default TrackChangeControlCommand
