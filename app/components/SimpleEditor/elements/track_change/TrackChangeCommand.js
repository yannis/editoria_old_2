import {AnnotationCommand} from 'substance'

class TrackChangeCommand extends AnnotationCommand {}

TrackChangeCommand.type = 'track-change'

export default TrackChangeCommand
