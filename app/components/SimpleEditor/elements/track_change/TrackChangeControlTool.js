import { Tool } from 'substance'

class TrackChangeControlTool extends Tool {

  getClassNames () {
    return 'sm-target-track-change-enable'
  }

  renderButton ($$) {
    const el = super.renderButton($$)

    let trackChangesMode = this.isTrackChangesOn()
    if (trackChangesMode === true) el.addClass('track-changes-active')

    if (!this.canAct()) el.attr('disabled', 'true')
    return el
  }

  isTrackChangesOn () {
    // TODO -- ??
    return this.parent._owner.props.trackChanges
  }

  canAct () {
    const provider = this.context.trackChangesProvider
    return provider.canAct()
  }
}

TrackChangeControlTool.type = 'track-change-enable'

export default TrackChangeControlTool
