import { Tool } from 'substance'

class TrackChangeControlViewTool extends Tool {

  getClassNames () {
    return 'sm-target-track-change-toggle-view'
  }
  renderButton ($$) {
    const el = super.renderButton($$)
    if (this.getViewMode()) el.addClass('track-changes-view-active')
    return el
  }

  getSurface () {
    const surfaceManager = this.context.surfaceManager
    const containerId = this.context.controller.props.containerId
    return surfaceManager.getSurface(containerId)
  }

  getViewMode () {
    const editor = this.context.editor
    const { trackChangesView } = editor.state

    return trackChangesView
  }
}

TrackChangeControlViewTool.type = 'track-change-toggle-view'

export default TrackChangeControlViewTool
