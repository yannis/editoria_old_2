// var NoStyleListItem = require('./NoStyleListItem')
// var NoStyleListItemComponent = require('./NoStyleListItemComponent')
// var NoStyleListEditing = require('./NoStyleListEditing')
//
// var ListMacro = require('substance/packages/list/ListMacro')
//
// module.exports = {
//   name: 'no-style-list-item',
//   configure: function (config, options) {
//     config.addNode(NoStyleListItem)
//     config.addComponent(NoStyleListItem.static.name, NoStyleListItemComponent)
//
//     config.addTextType({
//       name: 'no-style-list-item',
//       data: { type: 'no-style-list-item' }
//     })
//
//     config.addStyle(__dirname, '_noStyleList.scss')
//     config.addEditingBehavior(NoStyleListEditing)
//
//     if (options.enableMacro) {
//       config.addMacro(ListMacro)
//     }
//
//     config.addLabel('no-style-list', {
//       en: 'No Style List'
//     })
//
//     config.addLabel('no-style-list-item', {
//       en: 'Undecorated List'
//     })
//   }
// }
