// var NumberedListItem = require('./NumberedListItem')
// var NumberedListItemComponent = require('./NumberedListItemComponent')
// var NumberedListEditing = require('./NumberedListEditing')
//
// var ListMacro = require('substance/packages/list/ListMacro')
//
// module.exports = {
//   name: 'numbered-list-item',
//   configure: function (config, options) {
//     config.addNode(NumberedListItem)
//     config.addComponent(NumberedListItem.static.name, NumberedListItemComponent)
//
//     config.addTextType({
//       name: 'numbered-list-item',
//       data: { type: 'numbered-list-item' }
//     })
//
//     config.addStyle(__dirname, '_numberedList.scss')
//     config.addEditingBehavior(NumberedListEditing)
//
//     if (options.enableMacro) {
//       config.addMacro(ListMacro)
//     }
//
//     config.addLabel('numbered-list', {
//       en: 'Numbered List'
//     })
//
//     config.addLabel('numbered-list-item', {
//       en: 'Numbered List'
//     })
//   }
// }
