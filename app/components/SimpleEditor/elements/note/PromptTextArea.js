/* eslint react/prop-types: 0 */

import { Component } from 'substance'

class TextArea extends Component {
  render ($$) {
    const { disabled, path, placeholder, rows } = this.props

    const editorSession = this.context.editorSession
    const doc = editorSession.getDocument()
    const val = doc.get(path)

    const el = $$('textarea')
      .attr({
        // cols: this.props.columns || '40',
        placeholder: placeholder || 'Type your text here',
        rows: rows || '1'
      })
      .addClass('se-note-textarea')
      .append(val)
      .on('keyup', this.textAreaAdjust)

    if (disabled) el.attr('disabled', 'true')

    return el
  }

  textAreaAdjust (event) {
    let textArea = event.path[0]

    textArea.style.height = '1px'
    textArea.style.height = (textArea.scrollHeight) + 'px'
  }
}

export default TextArea
