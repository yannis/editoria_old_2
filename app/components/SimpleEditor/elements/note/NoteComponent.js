/* eslint react/prop-types: 0 */

import { Component } from 'substance'

class NoteComponent extends Component {
  render ($$) {
    const el = $$('note')
      .attr('note-content', this.props.node['note-content'])
      .addClass('sc-note')
      // .append('a')

    return el
  }

  dispose () {
    this.props.node.off(this)
  }
}

export default NoteComponent
