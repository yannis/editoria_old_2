import EditNoteTool from './EditNoteTool'
import Note from './Note'
import NoteCommand from './NoteCommand'
import NoteComponent from './NoteComponent'
import NoteHTMLConverter from './NoteHTMLConverter'
import NoteTool from './NoteTool'

export default {
  name: 'note',
  configure: function (config) {
    config.addNode(Note)

    config.addComponent(Note.type, NoteComponent)
    config.addConverter('html', NoteHTMLConverter)
    config.addCommand(Note.type, NoteCommand, { nodeType: Note.type })
    config.addTool('note', NoteTool, { toolGroup: 'annotations' })
    config.addTool('note', EditNoteTool, { toolGroup: 'overlay' })
    config.addIcon('note', { 'fontawesome': 'fa-bookmark' })
    config.addLabel('note', {
      en: 'Note'
    })
  }
}
