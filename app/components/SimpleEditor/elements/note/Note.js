import {InlineNode} from 'substance'

class Note extends InlineNode {}

Note.define({
  'type': 'note',
  'note-content': {
    type: 'string',
    optional: true
  }
})

export default Note
