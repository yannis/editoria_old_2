export default {
  type: 'note',
  tagName: 'note',

  import: function (element, node, converter) {
    node['note-content'] = element.attr('note-content')
  },

  export: function (node, element, converter) {
    var noteContent = node['note-content']
    element.setAttribute('note-content', noteContent)
  }
}
