import { InsertInlineNodeCommand } from 'substance'

class NoteCommand extends InsertInlineNodeCommand {
  createNodeData () {
    return {
      type: 'note'
    }
  }
}

NoteCommand.type = 'note'

export default NoteCommand
