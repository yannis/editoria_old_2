import Extract from './Extract'
import ExtractComponent from './ExtractComponent'
import ExtractHTMLConverter from './ExtractHTMLConverter'

export default {
  name: 'extract',
  configure: function (config) {
    config.addNode(Extract)

    config.addComponent(Extract.type, ExtractComponent)
    config.addConverter('html', ExtractHTMLConverter)

    config.addTextType({
      name: 'extract',
      data: {type: 'extract'}
    })

    config.addLabel('extract', {
      en: 'Extract'
    })
  }
}
