import { TextBlockComponent } from 'substance'

class ExtractComponent extends TextBlockComponent {
  didMount () {
    this.addClass('sc-blockquote')
  }
}

export default ExtractComponent
