import { TextBlock } from 'substance'

class Extract extends TextBlock {}

Extract.type = 'extract'

export default Extract
