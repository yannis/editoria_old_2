import { forEach, Tool } from 'substance'

class LeftSwitchTextTypeTool extends Tool {

  constructor (...args) {
    super(...args)
    let text = this.context.editorSession.configurator.config.textTypes
    this.extendProps({textTypes: text})
  }

  didMount (...args) {
    super.didMount(...args)
  }

  render ($$) {
    let labelProvider = this.context.labelProvider
    let el = $$('ul').addClass('sc-switch-text-type')

    forEach(this.props.textTypes, function (textType) {
      let item = $$('li')
          .addClass('sm-' + textType.spec.name)
          .addClass('se-option')
          .attr('data-type', textType.spec.name)
          .append(labelProvider.getLabel(textType.spec.name))
          .on('click', this.handleClick)

      item.ref('li-' + textType.spec.name)

      if (this.props.currentTextType &&
          textType.spec.name === this.props.currentTextType.name) {
        item.addClass('active')
      }

      el.append(item)
    }.bind(this))

    if ((this.props.currentTextType &&
        this.props.currentTextType.name === 'container-selection') ||
        this.isReadOnlyMode()
       ) {
      el.addClass('sm-disabled')
    }

    return el
  }

  executeCommand (textType) {
    this.context.commandManager.executeCommand(this.getCommandName(), {
      textType: textType
    })
  }

  handleClick (e) {
    e.preventDefault()
    if (this.isReadOnlyMode()) return
    this.executeCommand(e.currentTarget.dataset.type)
  }

  getSurface () {
    return this.context.surfaceManager.getSurface('body')
  }

  // TODO -- review how the side toolbar gets disabled
  isReadOnlyMode () {
    const surface = this.getSurface()
    if (!surface) return true         // HACK

    return surface.isReadOnlyMode()
  }
}

LeftSwitchTextTypeTool.command = 'switch-text-type'

export default LeftSwitchTextTypeTool
