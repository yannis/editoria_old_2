import {SwitchTextTypeCommand} from 'substance'
import LeftSwitchTextTypeTool from './LeftSwitchTextTypeTool'

export default {
  name: 'switch-text-type',
  configure: function (config, options) {
    config.addToolGroup('text')
    config.addCommand('switch-text-type', SwitchTextTypeCommand)
    config.addTool('switch-text-type', LeftSwitchTextTypeTool, {toolGroup: options.toolGroup || 'text'})
  }
}
