import React from 'react'
import { Redirect, Route } from 'react-router'

import { requireAuthentication } from 'pubsweet-frontend/src/components/AuthenticatedComponent'

// Manage
import Manage from 'pubsweet-component-manage/Manage'
import UsersManager from 'pubsweet-component-users-manager/UsersManager'
import TeamsManager from 'pubsweet-component-teams-manager/TeamsManager'
import Blog from 'pubsweet-component-blog/Blog'

// Editoria
import BookBuilder from './components/BookBuilder/BookBuilder'
import BookList from './components/BookBuilder/BookList'
import SimpleEditorWrapper from './components/SimpleEditor/SimpleEditorWrapper'

// Authentication
import Login from 'pubsweet-component-login/Login'
import Signup from 'pubsweet-component-signup/Signup'

const AuthenticatedManage = requireAuthentication(
  Manage, 'create', (state) => state.collections[0]
)

const AdminOnlyUsersManager = requireAuthentication(
  UsersManager, 'admin', (state) => state.collections[0]
)

const AdminOnlyTeamsManager = requireAuthentication(
  TeamsManager, 'admin', (state) => state.collections[0]
)

export default (
  <Route>
    <Redirect from='/' to='books' />
    <Redirect from='/manage/posts' to='books' />

    <Route path='/' component={AuthenticatedManage}>
      <Route path='books' component={BookList} />
      <Route path='blog' component={Blog} />
      <Route path='books/:id/book-builder' component={BookBuilder} />
      <Route path='books/:bookId/fragments/:fragmentId' component={SimpleEditorWrapper} />

      <Route path='users' component={AdminOnlyUsersManager} />
      <Route path='teams' component={AdminOnlyTeamsManager} />
    </Route>

    <Route path='/login' component={Login} />
    <Route path='/signup' component={Signup} />

    <Redirect path='*' to='books' />
  </Route>
)
